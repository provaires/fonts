# Exemples

Botarem aquí los exemples per aprendre a melhorar la qualitat de la traduccion.
- Abans tot
- Armonizacion
- Votz passiva
- Indicatiu - Imperatiu
- Errors de cercar

## Abans tot

Los espacis abans **! ?** e **:** son insecables, per evitar un retorn a la linha pels solets **! ?** o **:**

Passatz la mirga sul tèxte seguent e agachatz de remarcar la diferéncia entre l’espaci abant e aprèp lo signe.

> Mercé d’aver causit Firefox ! Per profeitar plenament de vòstre navigador, descobrissètz sas darrièras foncionalitats.

Per ara la traduccion de Firefox respecta pas complètament aquesta consigna.

Una recèrca dins Pontoon amb **?** tòrna aquò :

![Espacis] (/img/exemples/Espaci-insecable-normal.png "Diferéncia d’espacis")

Òm vei que solament la primièra frasa conten un espaci insecable, simbolizat amb un rectangle blau.

Serà una causa de cambiar per obténer una traduccion de las bonas.

## Armonizacion

### Estil general

Aquí avèm de cambiar doas causas per respectar l’estil general : un espaci mancant e una majusucla.

Atencion : s’agís d’un espaci insecable non pas un espaci ordinari.

![Captura 1](/img/exemples/espaci-majuscula-1.png "Captura dels marcapaginas per defaut de Firefox per Android")

![Captura 2](/img/exemples/espaci-majuscula-2.png "Indicacion de las causas de cambiar")

E lo resultat final un còp la correction facha amb l’interfàcia de traduccion de Mozilla.

![Captura 3](/img/exemples/espaci-majuscula-3.png "Correccion finala")

Un autre exemple d’armonizacion

## Votz passiva
Podèm traduire las formas passivas de l’anglés amb la meteissa estructura mas l’occitan aima pas la votz passiva.

 > Peer’s Certificate has been revoked by Firefox.
 
 Pòt donar **«Lo certificat del par es estat revocat per Firefox.»** o
 
 «Firefox a revocat lo certificat del par»
 
 Sovent avèm pas l’agent que realiza l’accion e sèm obligats a utilizar una votz passiva.
 
## Indicatiu - Imperatiu

La lenga anglesa indica pas clarament la diferéncia entre los dos mòdes e pòt èsser sorga d’errors.

Dins l’exemple seguent aquò se vei precisament perque trobam un mescladís dels dos mòdes, **Activar/Tornatz**. Cal ne causir un, aquí serà tot a l’indicatiu aprèp correccion.

![Captura 4](/img/exemples/Imperatiu-Indicatiu.png "Problèma de mòde")

## Acòrdis

De vegadas las cadenas son pas estadas traduchas al meteis temps e un acòrdi es marrit. Per dire d’evitar aqueles problèmas podèm agachar las traduccions de las autras lengas latinas.

A l’imatge seguent lo contèxte es **la** pagina d’acuèlh de Firefox. Podèm aver la pagina abituala o **personalizada**, non pas personalizat.

![Captura 5](/img/exemples/Acords.png "Problèma d’acòrdi")
 
## Errors de cercar
 
### Auxiliar Èsser 
 
 Es lo seu pròpri auxiliar al passat compausat
 
 
 Vòstre senhal a estat escafat => Vòstre senhal es estat escafat
 
 Totes los pòrts TCP an estats tampats => Totes los pòrts TCP son estats tampats
 
### Dempuèi
 
 En occitan general, estant que ven de l’usatge general en lengadocian, **dempuèi** s’utiliza amb una data o un ponch de partença.
 
 Podèm pas li apondre un lòc coma se pòt far en d’autres dialèctes.

 > This page is loaded from an extension.

 Cal traduire aital :
 
 > Aquela pagina es estada cargada d’una extension.
 
### Preposicions
 
 **Començar de** amb DE. Per trapar totas las cadenas de tèxtes amb aqueste vèrb, prepausi de picar «començ» e «comenc» pr’amor qu’amb la conjugaison pòdon pas perdre de temps en ensajant «comença» «començan, etc.
 