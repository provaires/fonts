# Exemples amb Firefox Focus

Aquesta aplicacion de Mozilla a per avantatge de conténer pas gaire de cadenas de tèxtes, l’analisi per trapar las fautas es fòrça rapid.

## Armonizacion

Lo cambiament automatizat de navigacion per navegacion a pas afectat la traduccion

![Navigacion] (/img/exemples/Focus_1.png "Dos biais d’escriure")

Cal anar a Pontoon e cercar «nav» per trapar totes los mots que començan per aquò e cambiar l’I per un E.

![Pontoon] (/img/exemples/Focus_2.png "Pontoon")

## Lista

Un exemple d’una lista de cambiar : avèm aquí un vèrb « recercar » puèi de noms « Vida privada », nos cal armonizar aquò en cambiar lo vèrb per un nom.

Aissí Recercar vendrà Recèrca

![Navigacion] (/img/exemples/Focus_lista-verb-nom.png "Logica lista")

## Vocabulari

Dempuèi lo 19/01/2018 s’utiliza pas que l’ortografia Drech e non pas Dreit. A l’imatge vesèm tanben un « navigador » de cambiar per « navegador ».

![Navigacion] (/img/exemples/Focus_voca.png "Dos biais d’escriure Drech/Dreit")

## Darrièras modificacions

Per acabar nòstre exemple, una lista sens logica (de vèrbs a l’imperatiu d’autre a l’indicatiu amb cambiament de persona). Dins aquesta lista un còp lo logicial s’adreça a l’utilizaire e de còps parla de Firefox en se.
Passem tot a l’imperatiu, e corrigem la fauta « Nòstre mission »

![Navigacion] (/img/exemples/Focus_logica_peca.png "Darrièras modificacions")