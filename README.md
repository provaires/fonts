# Los Provaires

![Pilot](img/test-pilot-firefox-occitan.png "Pilòt")

**Presentacion**

Aqueste repertòri liure ten per tòca de fornir de contenguts per ajudar a contribuire a la traduccion dels logicials desvolopats per la fondacion Mozilla.
Traparetz aquí d’explicacions per melhorar los tèxtes, las causidas lexicalas e las iniciativas per incitar a participar a l’utilizacion d’aisinas en lenga occitana.

## Ensenhador

- Aperitius traduccion
- Cicles de vida
- Corses
- Exemples
- Lexic
- Ligams

___


### Aperitius traduccion
L’objectiu màger d’aquesta amassada es de passar un moment per trapar las errors presentas dins los logicials. Pòt èsser una deca, un marrit contèxte, una cadena de tèxte tròp longa.
Lo monde se troba dins un lòc pendant una ora o doas, amb per manjar e beure.
Cal pas que utilizar los logicial e navigar pels menús. Un còp las errors reveladas manca pas que crear las suggestions de traduccion a l’interfàcia de traduccion de Mozilla apelada Pontoon.
En organizant d’eveniments d’aquesta mena, òm ensenha al monde cossí venir contributor mentre qu’òm passa un moment agradable entre occitanofòns.
Vist l’espendida del territòri occitan amai las mai mendres vilas pòdon aculhir una serada de traduccion.

### Cicles de vida
En çò de Mozilla un logicial deu passar par mantun cicles abans una mesa a disposicion pels canals diches «estables».

Per se trobar dins una version dicha establa, cal qu’una lenga siá mantenguda a jorn pendent 2 o 3 cicles de desvolopament.

Installar una version bêta pòt èsser malaisit pel public màger, una version establa demanda mens de parametratges e se trobar dirèctament a las botigas d’aplicacions dels sistèma mobils.

En pè de pagina podètz trapar un ligam per aprendre a installar una aplicacion fòra botiga.

___
#### Firefox per Android

traduch complètament lo 12 de novembre 2017 :


primièr cicle del 13/11/2017 al 22/01/2018 ✓

segond cicle del 23/01/2018 al 12/03/2018 ✓ 

tresen cicle del 13/03/2018 al 08/05/2018 : de verificar

___
#### Firefox per ordenador

traduch complètament lo 31 de decembre de 2017 :


primièr cicle del 23/01/2018 al 12/03/2018 ✓ 

segond cicle del 13/03/2018 al 08/05/2018 ✓ 

tresen cicle del 09/05/2018 al 02/07/2018 :✓ **version sortida**



### Corses
Per aprendre a utilizar l’interfàcia de Pontoon avètz aquí un cors pichon : http://sapiencia.eu/traduire-mozilla-firefox-en-occitan/

Autrament trapatz lo fichièr PDF aquí : https://framagit.org/provaires/fonts/blob/master/pdf/Traduire-Firefox-occitant.pdf

A partir de l’aisina de traduccion accedissètz als programas de Mozilla en cors de traduccion.

Pòt arribar que ne manca, cal demandar lor apondon.

Pel moment òm pòt participar a la traduccion de Firefox per ordenador, Firefox per Android, Firefox per iOS, Firefox Focus per Android e iOS.

Mas tanben a la traduccion del site de Mozilla. Lèu poirem tanben passar en occitan los ecrans de connexion al compte Mozilla.

En seguida trapatz dins la jos-seccion **Exemple** las causas de saber per melhorar la qualitat de las traduccions

### Exemples
Clicatz aquí per veire los exemples : https://framagit.org/provaires/fonts/blob/master/EXEMPLES.md

Clicatz aquí per veire los exemples amb Firefox Focus : https://framagit.org/provaires/fonts/blob/master/EXEMPLES_FOCUS.md

### De far

Completar la traduccion : 100% lo 19 de genièr de 2018

Manténer la traduccion : totjorn

#### Armonizar vocabulari : en cors

___

Armonizar parelhs « **drech** / dreit /  **ponch** / punt / **plànher** / plànger » : cercar la rèsta de las parelhs. Prendre lo lengadocian fòra occidental es la causida de Josiana UBAUD


| Parelh        | Armonizacion facha lo    | Nòtas  |
| ------------- |:------------------------:| ------:|
| ponch/punt    | 19/01/2018  | Traduire «pointer» pontaire ? |
| drech/dreit   | 19/01/2018       | Tanben «dreita» e «endreit»       |
| dobrir/dubrir | 25/01/2018 | Tanben pels participis passats |
| pas mai/pas pus | 28/01/2018 | Veire la pagina **Exemples** |
| … / ...       | 29/01/2018   | 130 ocurréncias cambiadas |
| (a)profeitar/(a)profechar ?|    |
| navegador/navigator o navigador ? | 26/02/2018  | Amb E e D |
| quichapapièrs/cachapapièrs | 18/04/2018 | |
|compòrtament/comportament | 26/05/2018 | Amb un Ò|
|*mantatòri->mandatari|24/06/2018| Non èra pas corrècte|

Armonizar parelhs « subre / sobre, bufar / bofar » aquí s’agís pas de prendre pas lo lengadocian occidental.

Vèrbs a alternança : Tòrna/Torna

Armonizar plurals : utilizar lo plurial sensible pertot.

Causir entre : - autocomplecion e emplenatge autormatic, performànça, performancia, rendiment

___

### Lexic
Una tièrra de paralaus frequentas amb la causida finala pels projèctes de Mozilla.

Aquesta lista ten per objectiu d’omogeneïzar la traduccion, vòl pas dire que los demai mòts sián incorrèctes coma : recerca / recèrca


| Anglés      | Occitan         | Nòtas  |
| ------------- |:-------------:| ------:|
| add-on        | modul         | ~ complementari |
| password      | senhal        |        |
| camera        | camèra        | amb È non pas À |
| to close      | tampar        |        |
| computer      | ordenador     | amb E e D, es una aisina non pas una persona|
| device        | periferic     |        |
| to track      | pistar        |        |
| tracker       | traçador      |        |
| tracking      | seguiment     |        |
| to refresh    | actualizar    |        |
| to restart    | reaviar       |sul modèl de **Reaver**, «tornar aviar» dins una frasa longa |
| to restore    | restablir     | restablissament per l’accion |
| to save       | enregistrar   | **Salvar** pels senhals e identificants, **Gardar** tanben dins los dos cases |
| search        | recèrca       | amb È  |

### Ligams
Interfàcia de traduccion Pontoon : https://pontoon.mozilla.org/oc/

Interfàcia per trobar los bugs : https://l10n.mozilla.org/teams/oc#bugzilla

Libre d’estil : https://www.softcatala.org/guia-estil-de-softcatala/tota-la-guia/

Cors traduccion en linha : http://sapiencia.eu/traduire-mozilla-firefox-en-occitan/

Installar una version bêta : https://opinion.jornalet.com/lo-quentin/blog/2418/firefox-per-android

Telecargar Firefox bêta per ordenador : https://www.mozilla.org/fr/firefox/nightly/all/?q=occitan

Lèu los ligams de malhums socials

**Crèdits : Quentin PAGÈS**
